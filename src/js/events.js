module.exports = [
  {
    "start": "9:00",
    "end": "10:00",
    "item": "Morning Tea & Check In",
    "description": "",
    "speakers": []
  },
  {
    "start": "10:00",
    "end": "10:40",
    "item": "Welcome Note, Inauguration, Opening Remarks",
    "description": "",
    "speakers": [2,3,4]
  },
  {
    "start": "10:45",
    "end": "10:55",
    "item": "Startup India Hub",
    "description": "#### Presentation by Invest India Team",
    "speakers": [5]
  },
  {
    "start": "10:55",
    "end": "11:05",
    "item": "Tea Break",
    "description": "",
    "speakers": []
  },
  {
    "start": "11:05",
    "end": "11:20",
    "item": "Implementation of State Startup Policy",
    "description": "#### Presentation on Kerala Startup Ecosystem",
    "speakers": [4]
  },
  {
    "start": "11:20",
    "end": "12:20",
    "item": "Panel Discussion",
    "description": "#### Enablers of Startup Ecosystem Development\n* Institutional Support(Nodal Team,Implementation method, Knowledge enhancement for the team)\n* Simplification of Regulations (regulatory Support & Tracking the startups with grievances addressed)\n* Awareness and Outreach (Output oriented discussion like success stories of gen,Neuroplex, Lamara etc)",
    "speakers": [9,7,8,6,10]
  },
  {
    "start": "12:20",
    "end": "12:30",
    "item": "Tea Break",
    "description": "",
    "speakers": []
  },
  {
    "start": "12:30",
    "end": "14:00",
    "item": "Panel Discussion",
    "description": "#### Role played by Incubators, Accelerators and other Enablers \nin the journey of Startups through \ndifferent stages of their lifecycle",
    "speakers": [11,12,13,14,15,16]
  },
  {
    "start": "14:00",
    "end": "14:45",
    "item": "Networking Lunch",
    "description": "",
    "speakers": []
  },
  {
    "start": "14:45",
    "end": "15:20",
    "item": "Travel to Kerala Technology Innovation Zone, Cochin",
    "description": "",
    "speakers": []
  },
  {
    "start": "15:20",
    "end": "18:30",
    "item": "Field Visit",
    "description": "* Integrated Startup Complex\n* Maker Village\n* Fab Lab Kochi\n* Startup Product showcasing and Demo",
    "speakers": []
  },
  {
    "start": "18:30",
    "end": "18:50",
    "item": "Heading Back to Hotel",
    "description": "",
    "speakers": []
  },
  {
    "start": "19:30",
    "end": "22:00",
    "item": "GALA DINNER & Networking",
    "description": "",
    "speakers": []
  }
]