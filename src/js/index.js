import jQuery from "jquery";
import popper from "popper.js";
import bootstrap from "bootstrap";
import SmoothScroll from 'smooth-scroll';
import magnificPopup from 'magnific-popup';

window.$ = window.jQuery = jQuery;
window.bootstrap = bootstrap;
window.popper = popper;

// $('.video-pop-up').magnificPopup({
//   type: 'iframe',
//   mainClass: 'mfp-fade',
//   removalDelay: 160,
//   preloader: false,
//   fixedContentPos: false
// });

// $.magnificPopup.open({
//   items: {
//     src: 'reshed.webp'
//   },
//   type: 'image'
// });

// $('.img-pop-up').magnificPopup({
//   type: 'image',
//   gallery: {
//       enabled: true
//   }
// });

var pathname = location.pathname.split("/");
$('#footer-menu a[href="' + pathname[pathname.length-1] + '"]').addClass('active-nav');
