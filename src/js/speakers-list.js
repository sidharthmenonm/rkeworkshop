module.exports = [
  {
    "id": 1,
    "order": 0,
    "name": "Kerala Startup Mission Team",
    "designation": "",
    "company": "",
    "image": "ksum.jpg"
  },
  {
    "id": 2,
    "order": 1,
    "name": "Dr. Christy Fernandez IAS (Retd.)",
    "designation": "Chairman, KSIDC",
    "company": "Former Secretary to President of India",
    "image": "cristy.jpg"
  },
  {
    "id": 3,
    "order": 2,
    "name": "Smt. Shruti Singh IAS",
    "designation": "Director, DPIIT",
    "company": "Govt. of India",
    "image": "sruthi.jpg"
  },
  {
    "id": 4,
    "order": 3,
    "name": "Dr. Saji Gopinath",
    "designation": "CEO",
    "company": "Kerala Startup Mission",
    "image": "sajigopinath.jpg"
  },
  {
    "id": 5,
    "order": 0,
    "name": "Invest India Team",
    "designation": "",
    "company": "",
    "image": "invind.jpg"
  },
  {
    "id": 6,
    "order": 4,
    "name": "Shri. Tom Thomas",
    "designation": "Director Operations",
    "company": "Kerala Startup Mission",
    "image": "tom.jpg"
  },
  {
    "id": 7,
    "order": 5,
    "name": "Shri. Sathyanarayana",
    "designation": "Deputy Head",
    "company": "K-Tech Innovation Hub",
    "image": "sathya.jpg"
  },
  {
    "id": 8,
    "order": 6,
    "name": "Shri. Prasanta Biswal",
    "designation": "Evangelist",
    "company": "Startup Odisha",
    "image": "prasantha.jpg"
  },
  {
    "id": 9,
    "order": 7,
    "name": "Shri. Sanjay Vijayakumar",
    "designation": "Member Startup Implementation Committee",
    "company": "AICTE & CEO, SV.CO",
    "image": "sanjay.jpg"
  },
  {
    "id": 10,
    "order": 8,
    "name": "Shri. Santhosh Kurup",
    "designation": "CEO",
    "company": "ICT Academy of Kerala",
    "image": "ict.jpg"
  },
  {
    "id": 11,
    "order": 9,
    "name": "Shri. Jitender Minhas",
    "designation": "CEO",
    "company": "IAMAI (Mobile 10X)",
    "image": "jithender.jpg"
  },
  {
    "id": 12,
    "order": 10,
    "name": "Dr. Abraham T Mathew",
    "designation": "Professor, NIT Calicut",
    "company": "Founder Chairman, NIT TBI",
    "image": "abraham.jpg"
  },
  {
    "id": 13,
    "order": 11,
    "name": "Shri Ajay Ramasubramanium",
    "designation": "Former CEO,",
    "company": "Zone Startups India",
    "image": "ajay.jpg"
  },
  {
    "id": 14,
    "order": 12,
    "name": "Dr George Nainan",
    "designation": "Principal Scientist",
    "company": "CIFT",
    "image": "george.jpg"
  },
  {
    "id": 15,
    "order": 13,
    "name": "Smt Surya Thankam",
    "designation": "Manager (Incubation)",
    "company": "Kerala Startup Mission",
    "image": "surya.jpg"
  },
  {
    "id": 16,
    "order": 14,
    "name": "Shri Prasad Balakrishnan Nair",
    "designation": "CEO",
    "company": "Maker Village",
    "image": "maker.jpg"
  },
  {
    "id": 17,
    "order": 15,
    "name": "Shri Vishesh Rajaram",
    "designation": "Managing Partner",
    "company": "Speciale Invest",
    "image": "vishesh.jpg"
  },
  {
    "id": 18,
    "order": 16,
    "name": "Shri Nagaraja Prakasam",
    "designation": "Partner, Acumen Fund & Angel Investor",
    "company": "Indian Angel Network",
    "image": "naga.jpg"
  },
  {
    "id": 19,
    "order": 17,
    "name": "Shri Shilen Sagunan",
    "designation": "Chairman",
    "company": "Malabar Innovation Zone & Malabar Angels",
    "image": "shilen.jpg"
  },
  {
    "id": 20,
    "order": 18,
    "name": "Shri Anil Joshi",
    "designation": "Managing Partner",
    "company": "Unicorn India Ventures",
    "image": "anil.jpg"
  },
  {
    "id": 21,
    "order": 19,
    "name": "Shri Muhamed Riyas",
    "designation": "Director Funding & Global Linkages",
    "company": "Kerala Startup Mission",
    "image": "riyas.jpg"
  },
  {
    "id": 22,
    "order": 20,
    "name": "Shri Sijo Kuruvila George",
    "designation": "Consultant",
    "company": "Lets Venture",
    "image": "sijo.jpg"
  },
  {
    "id": 23,
    "order": 21,
    "name": "Shri Anand Sarma",
    "designation": "Director",
    "company": "KPMG",
    "image": "anand.jpg"
  },
  {
    "id": 24,
    "order": 22,
    "name": "Shri Dinesh Thampi",
    "designation": "Vice President & Delivery Centre Head",
    "company": "Tata Consultancy Services",
    "image": "dinesh.jpg"
  },
  {
    "id": 25,
    "order": 23,
    "name": "Shri Prasad Paniker",
    "designation": "Executive Director",
    "company": "BPCL",
    "image": "prasad.jpg"
  },
  {
    "id": 26,
    "order": 24,
    "name": "Shri Aravind Krishnaswamy",
    "designation": "Former Executive Director, Corporate & Digital Strategy",
    "company": "BPCL",
    "image": "aravind.jpg"
  },
  {
    "id": 27,
    "order": 25,
    "name": "Shri Binu Rajendran",
    "designation": "Director",
    "company": "VKC",
    "image": "binu.jpg"
  },
  {
    "id": 28,
    "order": 26,
    "name": "Shri Hrishikesh Nair",
    "designation": "CEO",
    "company": "Kerala IT Parks",
    "image": "hrishikesh.jpg"
  },
  {
    "id": 29,
    "order": 27,
    "name": "Dr Moni Abraham",
    "designation": "Director",
    "company": "Cochin Cancer Research Institute",
    "image": "moni.jpg"
  },
  {
    "id": 30,
    "order": 28,
    "name": "Shri Ashok Kurian Panjikaran",
    "designation": "Manager (BD)",
    "company": "Kerala Startup Mission",
    "image": ""
  },
  {
    "id": 31,
    "order": 29,
    "name": "Shri C Balagopal",
    "designation": "Director",
    "company": "Federal Bank",
    "image": "federal.jpg"
  },
  {
    "id": 32,
    "order": 30,
    "name": "Shri Madhavan Nambiar IAS (Retd)",
    "designation": "Chairman, Hi Power IT Committee, Govt of Kerala",
    "company": "Chairman, IIITMK and Maker Village.",
    "image": "madhavan.jpg"
  }
]