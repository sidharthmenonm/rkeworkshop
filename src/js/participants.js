import Vue from 'vue';
import speaker_list from './participant-list';

new Vue({ 
  el: '#speaker-section',
  data: {
    speakers : speaker_list
  },
  computed: {
    // sorted: function(){
    //   return this.sort(this.speakers)
    // }
  },
  methods: {
    // sort: function(arr) {
    //   // Set slice() to avoid to generate an infinite loop!
    //   var sorted = arr.slice().sort(function(a, b) {
    //     return a.order - b.order;
    //   });

    //   return sorted.filter(function(item){
    //     return item.order!=0;
    //   })
    // }
  },
})