module.exports = [
  {
    "state": "A&N Islands",
    "name": "Ajit Anand",
    "designation": "Director of Industries"
  },
  {
    "state": "A&N Islands",
    "name": "Shahid Khan",
    "designation": "Industries Promotion Officer"
  },
  {
    "state": "Assam",
    "name": "Meenakshi Sundaram IAS",
    "designation": "Commissioner & Secretary, I&C Deptt."
  },
  {
    "state": "Assam",
    "name": "Pranjal Konwar",
    "designation": "COO, Assam Startup The Nest"
  },
  {
    "state": "Puducherry",
    "name": "Thiru G.Ramakrishnan",
    "designation": "Assistant Director"
  },
  {
    "state": "Puducherry",
    "name": "Thiru V.Amudhakumar",
    "designation": "Project Manager, DIC"
  },
  {
    "state": "Jharkhand",
    "name": "Umesh Shah",
    "designation": "Director of IT Department"
  },
  {
    "state": "Jharkhand",
    "name": "Sumit Kumar",
    "designation": "Vice President, AB Vajpayee Innovation Lab"
  },
  {
    "state": "Jharkhand",
    "name": "Rachna Bhardawaj",
    "designation": "Manager, AB Vajpayee Innovation Lab"
  },
  {
    "state": "Jharkhand",
    "name": "Rashmi",
    "designation": "Consultant"
  },
  {
    "state": "Chhattisgarh",
    "name": "Sanjay Rane",
    "designation": "Dy. Director, Directorate of Industries"
  },
  {
    "state": "Chhattisgarh",
    "name": "Praveen V",
    "designation": "Startup Chhattisgarh Team"
  },
  {
    "state": "Chhattisgarh",
    "name": "Prabbu Menon",
    "designation": "Startup Chhattisgarh Team"
  },
  {
    "state": "Madhya Pradesh",
    "name": "V.C Dubey",
    "designation": "Nodal Officer, Startup & Incubation Cell"
  },
  {
    "state": "Madhya Pradesh",
    "name": "Divya Sharma Godha",
    "designation": "Consultant"
  },
  {
    "state": "Madhya Pradesh",
    "name": "Ankur Yelne",
    "designation": "Consultant"
  },
  {
    "state": "Sikkim",
    "name": "Dr. Thomas Chandy",
    "designation": "Additional Chief Secretary"
  },
  {
    "state": "Sikkim",
    "name": "S.K Pradhan",
    "designation": "Secretary"
  },
  {
    "state": "Sikkim",
    "name": "N.Jaswant",
    "designation": "Joint Secretary"
  },
  {
    "state": "Arunachal Pradesh",
    "name": "Habung Donyi",
    "designation": "Director of Industries"
  },
  {
    "state": "Arunachal Pradesh",
    "name": "Goli Angu",
    "designation": "Deputy Director of Industries"
  },
  {
    "state": "Nagaland",
    "name": "Lithrongla G. Chishi IAS",
    "designation": "Commissioner & Secretary, I&C Deptt."
  },
  {
    "state": "Nagaland",
    "name": "Er. YL Thongtsar",
    "designation": "Deputy Director, I&C Deptt."
  },
  {
    "state": "Nagaland",
    "name": "Er. Keneireinuo K. Theunuo",
    "designation": "Assitant Director, I&C Deptt."
  },
  {
    "state": "Odisha",
    "name": "Pranab Jyoti Nath IAS",
    "designation": "Director of Industries"
  },
  {
    "state": "Odisha",
    "name": "Prasanta Biswal",
    "designation": "Evangelist, Startup Odisha"
  },
  {
    "state": "Odisha",
    "name": "Rashmi Ranjan Sahu",
    "designation": "Senior Mission Associate"
  },
  {
    "state": "Bihar",
    "name": "Ravindra Prasad",
    "designation": "Director, Technical Development"
  },
  {
    "state": "Bihar",
    "name": "Balram Singh",
    "designation": "Joint Director, Industries"
  },
  {
    "state": "Maharashtra",
    "name": "Hiten",
    "designation": "Assistant Manager, MSInS"
  },
  {
    "state": "Maharashtra",
    "name": "Devendra",
    "designation": "Manager, MSInS"
  },
  {
    "state": "Karnataka",
    "name": "Sathyanarayana B V",
    "designation": "Deputy Head, Startup Karnataka, KITS"
  },
  {
    "state": "Karnataka",
    "name": "Jaikrishna R",
    "designation": "Manager, Startup Karnataka, KITS"
  },
  {
    "state": "Invest India",
    "name": "Preet Deep Singh",
    "designation": "Co-Head"
  },
  {
    "state": "Invest India",
    "name": "Palak",
    "designation": "Manager"
  },
  {
    "state": "Invest India",
    "name": "Saransh Roy",
    "designation": "Asst. Manager"
  },
  {
    "state": "Invest India",
    "name": "Pratham",
    "designation": "Manager"
  },
  {
    "state": "Invest India",
    "name": "Ati Malik",
    "designation": "Asst. Mangager"
  },
  {
    "state": "Invest India",
    "name": "Sagar Sengar",
    "designation": "Associate"
  },
  {
    "state": "DPIIT",
    "name": "Shruti Singh IAS",
    "designation": "Director"
  },
  {
    "state": "DPIIT",
    "name": "LMK Reddy",
    "designation": "Under Secretary"
  },
  {
    "state": "DPIIT",
    "name": "Akhil Rai",
    "designation": "Section Officer"
  }
]