module.exports = [
  {
    "start": "9:45",
    "end": "11:00",
    "item": "Panel Discussion",
    "description": "* Early Stage Funding Landscape in Tier 1, Tier 2, and Tier 3 Cities\n* Government as a Startup Investor: Models,  Opportunities & Risks\n* Interventions for Activating Regional Angel Investor Networks",
    "speakers": [17,18,19,20,21,22]
  },
  {
    "start": "11:15",
    "end": "12:15",
    "item": "Fireside Chat",
    "description": "Government as a Marketplace (Easing Public procurement)\n1. Demand Day\n1. Demo Day\n1. Innovation Zone\n1. Direct Procurement",
    "speakers": [4,23]
  },
  {
    "start": "",
    "end": "",
    "item": "Case study and presentation of startups.",
    "description": "1. Genrobotics\n1. Pinpark\n1. Thought Ripples",
    "speakers": []
  },
  {
    "start": "12:30",
    "end": "13:45",
    "item": "Panel Discussion",
    "description": "* Building Niche Market for startups\n* Corporate Innovation Programmes\n* Startup Case Studies",
    "speakers": [24,25,26,27,28,29,31]
  },
  {
    "start": "13:45",
    "end": "14:00",
    "item": "Valedictory",
    "description": "",
    "speakers": [32,3]
  },
  {
    "start": "14:00",
    "end": "15:00",
    "item": "Lunch Break",
    "description": "",
    "speakers": []
  },
  {
    "start": "15:10",
    "end": "15:30",
    "item": "Heading to Bio Nest, Kochi",
    "description": "",
    "speakers": []
  },
  {
    "start": "15:30",
    "end": "16:15",
    "item": "Field Visit",
    "description": "Bionest",
    "speakers": []
  },
  {
    "start": "16:15",
    "end": "17:00",
    "item": "Field Visit",
    "description": "Startup presentation at INQ",
    "speakers": []
  },
  {
    "start": "17:00",
    "end": "17:00",
    "item": "",
    "description": "Winding up and heading back to hotel",
    "speakers": []
  }
]